module gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2

go 1.12

require (
	github.com/logrusorgru/aurora v0.0.0-20191017060258-dc85c304c434
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.6.1
)
