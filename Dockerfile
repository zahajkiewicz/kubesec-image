FROM golang:1.13 AS build
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY . .

RUN go build -o analyzer
#RUN wget --no-check-certificate -P /tmp https://weblogin.f5net.com/sso/certs/F5NET_Certificate-Bundle.p7b

# The "app" user (from the kubesec/kubesec image) doesn't have permission to create a
# file in /etc/ssl/certs, this RUN command creates a file that the analyzer can add
# additional ca certs to trust.
RUN touch /ca-cert-additional-gitlab-bundle.pem

FROM kubesec/kubesec:57f15fc

ENV PATH="/home/app:${PATH}"
COPY --from=build /go/src/app/analyzer /
COPY --from=build --chown=app:app /ca-cert-additional-gitlab-bundle.pem /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem

ENTRYPOINT []
CMD ["/analyzer", "run"]
