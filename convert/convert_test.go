package convert

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestConvert(t *testing.T) {
	filepath1 := "simple_manifest.yml"
	report1 := `[
    {
      "object": "DaemonSet/fluentd.kube-system",
      "valid": true,
      "message": "Failed with a score of -15 points",
      "score": -15,
      "scoring": {
        "critical": [
          {
            "selector": ".spec .hostNetwork == true",
            "reason": "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter"
          },
          {
            "selector": ".spec .hostPID == true",
            "reason": "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration"
          }
        ],
        "advise": [
          {
            "selector": "containers[] .securityContext .capabilities .drop",
            "reason": "Reducing kernel capabilities available to a container limits its attack surface"
          }
        ]
      }
    }
  ]`

	filepath2 := "yaml.yaml"
	report2 := `[
    {
      "object": "DaemonSet/fluentd.kube-system",
      "valid": true,
      "message": "Failed with a score of -15 points",
      "score": -15,
      "scoring": {
        "critical": [
          {
            "selector": ".spec .hostNetwork == true",
            "reason": "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter"
          },
          {
            "selector": ".spec .hostPID == true",
            "reason": "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration"
          }
        ],
        "advise": [
          {
            "selector": "containers[] .securityContext .capabilities .drop",
            "reason": "Reducing kernel capabilities available to a container limits its attack surface"
          }
        ]
      }
    }
  ]`

	var vulnerability1 = KubesecOutput{
		Filepath: filepath1,
		Findings: []byte(report1),
	}

	var vulnerability2 = KubesecOutput{
		Filepath: filepath2,
		Findings: []byte(report2),
	}

	var vulnerabilities = make([]KubesecOutput, 0)
	vulnerabilities = append(vulnerabilities, vulnerability1)
	vulnerabilities = append(vulnerabilities, vulnerability2)

	file, _ := json.MarshalIndent(vulnerabilities, "", " ")

	r := ioutil.NopCloser(bytes.NewReader(file))

	scanner := issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	want := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        ".spec .hostNetwork == true",
				Message:     "DaemonSet/fluentd.kube-system .spec .hostNetwork == true",
				Description: "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter",
				// the selector
				CompareKey: "simple_manifest.yml:DaemonSet/fluentd.kube-system:.spec.hostNetwork==true",
				Severity:   issue.SeverityLevelCritical,
				Confidence: issue.ConfidenceLevelHigh,
				Location: issue.Location{
					File:  "simple_manifest.yml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "Kubesec Rule ID .spec .hostNetwork == true",
						Value: "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter",
					},
				},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        ".spec .hostPID == true",
				Message:     "DaemonSet/fluentd.kube-system .spec .hostPID == true",
				Description: "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration",
				// the selector
				CompareKey: "simple_manifest.yml:DaemonSet/fluentd.kube-system:.spec.hostPID==true",
				Severity:   issue.SeverityLevelCritical,
				Confidence: issue.ConfidenceLevelHigh,
				Location: issue.Location{
					File:  "simple_manifest.yml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "Kubesec Rule ID .spec .hostPID == true",
						Value: "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration",
					},
				},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "containers[] .securityContext .capabilities .drop",
				Message:     "DaemonSet/fluentd.kube-system containers[] .securityContext .capabilities .drop",
				Description: "Reducing kernel capabilities available to a container limits its attack surface",
				CompareKey:  "simple_manifest.yml:DaemonSet/fluentd.kube-system:containers[].securityContext.capabilities.drop",
				Severity:    issue.SeverityLevelInfo,
				Confidence:  issue.ConfidenceLevelHigh,
				Location: issue.Location{
					File:  "simple_manifest.yml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "Kubesec Rule ID containers[] .securityContext .capabilities .drop",
						Value: "Reducing kernel capabilities available to a container limits its attack surface",
					},
				},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        ".spec .hostNetwork == true",
				Message:     "DaemonSet/fluentd.kube-system .spec .hostNetwork == true",
				Description: "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter",
				// the selector
				CompareKey: "yaml.yaml:DaemonSet/fluentd.kube-system:.spec.hostNetwork==true",
				Severity:   issue.SeverityLevelCritical,
				Confidence: issue.ConfidenceLevelHigh,
				Location: issue.Location{
					File:  "yaml.yaml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "Kubesec Rule ID .spec .hostNetwork == true",
						Value: "Sharing the host's network namespace permits processes in the pod to communicate with processes bound to the host's loopback adapter",
					},
				},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        ".spec .hostPID == true",
				Message:     "DaemonSet/fluentd.kube-system .spec .hostPID == true",
				Description: "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration",
				// the selector
				CompareKey: "yaml.yaml:DaemonSet/fluentd.kube-system:.spec.hostPID==true",
				Severity:   issue.SeverityLevelCritical,
				Confidence: issue.ConfidenceLevelHigh,
				Location: issue.Location{
					File:  "yaml.yaml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "Kubesec Rule ID .spec .hostPID == true",
						Value: "Sharing the host's PID namespace allows visibility of processes on the host, potentially leaking information such as environment variables and configuration",
					},
				},
			},
			{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        "containers[] .securityContext .capabilities .drop",
				Message:     "DaemonSet/fluentd.kube-system containers[] .securityContext .capabilities .drop",
				Description: "Reducing kernel capabilities available to a container limits its attack surface",
				CompareKey:  "yaml.yaml:DaemonSet/fluentd.kube-system:containers[].securityContext.capabilities.drop",
				Severity:    issue.SeverityLevelInfo,
				Confidence:  issue.ConfidenceLevelHigh,
				Location: issue.Location{
					File:  "yaml.yaml",
					Class: "DaemonSet/fluentd.kube-system",
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  "Kubesec Rule ID containers[] .securityContext .capabilities .drop",
						Value: "Reducing kernel capabilities available to a container limits its attack surface",
					},
				},
			},
		},
		Remediations:    []issue.Remediation{},
		DependencyFiles: []issue.DependencyFile{},
	}

	got, err := Convert(r, "")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
