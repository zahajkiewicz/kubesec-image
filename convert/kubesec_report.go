package convert

import (
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

type KubesecOutput struct {
	Filepath string `json:"filepath"`
	Findings []byte `json:"findings"`
}

type KubesecReport struct {
	Filepath string          `json:"filepath"`
	Findings KubesecFindings `json:"findings"`
}

type KubesecFindings struct {
	Object  string         `json:"object"`
	Message string         `json:"message"`
	Scoring KubesecScoring `json:"scoring"`
}

type KubesecScoring struct {
	CriticalSeverity []KubesecVulnerability `json:"critical"`
	InfoSeverity     []KubesecVulnerability `json:"advise"`
}

type KubesecVulnerability struct {
	Object     string
	Selector   string
	Reason     string
	Confidence issue.ConfidenceLevel
	Severity   issue.SeverityLevel
	Filepath   string
}

func (r *KubesecReport) vulnerabilities() []KubesecVulnerability {
	var vulnerabilities []KubesecVulnerability

	for _, vulnerability := range r.Findings.Scoring.CriticalSeverity {
		vulnerability.Object = r.Findings.Object
		vulnerability.Confidence = issue.ConfidenceLevelHigh
		vulnerability.Severity = issue.SeverityLevelCritical
		vulnerability.Filepath = r.Filepath
		vulnerabilities = append(vulnerabilities, vulnerability)
	}

	for _, vulnerability := range r.Findings.Scoring.InfoSeverity {
		vulnerability.Object = r.Findings.Object
		vulnerability.Confidence = issue.ConfidenceLevelHigh
		vulnerability.Severity = issue.SeverityLevelInfo
		vulnerability.Filepath = r.Filepath
		vulnerabilities = append(vulnerabilities, vulnerability)
	}

	return vulnerabilities
}

// compareKey returns a string used to establish whether two issues are the same.
func (v *KubesecVulnerability) compareKey() string {
	attrs := []string{v.Filepath, v.Object, strings.Replace(v.Selector, " ", "", -1)}
	return strings.Join(attrs, ":")
}
