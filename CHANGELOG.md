# Kubesec analyzer changelog

## v2.1.0
- Add support for custom CA certs (!7)

## v2.0.1
- Move away from use of CI_PROJECT_DIR variable

## v2.0.0
- Initial release
